# Accessing a Host via SSH on a Specific Port
> provided by: [Raúl Ortega](https://gitlab.com/raul.ortega)

## Objective

This tutorial will guide you step-by-step to access a remote server via SSH using a specific port. In this example, we will use port **32117**, the user **emma**, and the host with IP address **192.168.0.24**.

## Requirements

- **SSH** installed on your local machine (Linux/macOS usually have it pre-installed, on Windows you can use tools like **PuTTY** or **PowerShell**).
- Access to the network where the host **192.168.0.24** is located.
- SSH access credentials, i.e., the username (**emma**) and the password or SSH private key.

## Basic Command to Connect

The basic command to connect via SSH to a server on port 32117 is:

```bash
ssh -p 32117 emma@192.168.0.24
```

## Command Explanation

- **ssh**: The command to start an SSH session.
- **-p 32117**: The `-p` option specifies the port to use for the connection. In this case, it's **32117**.
- **emma**: The username to connect to the remote host.
- **192.168.0.24**: The IP address of the host you want to access.

## Step-by-Step Instructions

### 1. Open the Terminal (Linux/macOS)

On **Linux** or **macOS**, open the terminal. On **Windows**, you can use **PowerShell** or an SSH client like **PuTTY**.

### 2. Execute the SSH Command

In the terminal, type the following command to connect to the remote server:

```bash
ssh -p 32117 emma@192.168.0.24
```

### 3. Accept the Host's Fingerprint (First Time Only)

The first time you connect to a server, you'll see a message like the following:

```
The authenticity of host '192.168.0.24 (192.168.0.24)' can't be established.
ECDSA key fingerprint is SHA256:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.
Are you sure you want to continue connecting (yes/no)?
```

This message warns you that it's the first time you're connecting to this server. Confirm that you trust it.

Type **yes** and press **Enter**.

### 4. Enter the Password

If you are not using SSH keys, the system will prompt you to enter the password for the user **emma**. Type the password and press **Enter**.

Note: For security reasons, while typing the password, no characters will be displayed (not even asterisks), but your input is being recorded.

### 5. Connection Established

If the password is correct, you will see something like this:

```
Last login: Wed Oct 10 13:35:20 2024 from 192.168.0.100
emma@remote-host:~$
```

You are now connected to the remote server! You can execute commands on the server as if you were working locally.

## Connect Using an SSH Key (optional)

Instead of entering a password each time, you can use SSH keys for a more secure authentication.

### Step 1: Generate an SSH Key

If you don't already have an SSH key pair generated, you can do so with the following command in your terminal:

```bash
ssh-keygen -t rsa -b 4096
```

Follow the on-screen instructions. This will generate two files in the `~/.ssh/` directory:

- `id_rsa`: Your private key (do not share this).
- `id_rsa.pub`: Your public key (this is the one you share with the server).

### Step 2: Copy the Public Key to the Server

To copy your public key to the remote server, use the following command:

```bash
ssh-copy-id -p 32117 emma@192.168.0.24
```

This will automatically copy your public key to the correct location on the remote server. From now on, you can connect without entering a password.

### Step 3: Connect Without a Password

Once the public key is copied, you can connect to the remote server using the same SSH command, but no password will be required:

```bash
ssh -p 32117 emma@192.168.0.24
```

## Troubleshooting

- **Connection Error**: If you see a message like `Connection refused`, ensure that:
  - The SSH service is running on the remote server.
  - Port 32117 is open and accessible.
  - You have access to the correct network (VPN, local network, etc.).

- **Wrong Password Error**: If you enter the wrong password multiple times, the server may temporarily block your IP. Make sure you have the correct credentials or use SSH keys for more secure and streamlined access.

## Conclusion

This tutorial covers how to connect to a remote server via SSH on a specific port, in this case, **port 32117** with the user **emma** and host **192.168.0.24**. You also learned how to use SSH keys to simplify and secure the connection process.

