# Running Multiple Replicas of an R Script using Kubernetes Jobs

> provided by: [Raúl Ortega](https://gitlab.com/raul.ortega)

This guide will walk you through running multiple replicas of an R script on a Kubernetes cluster. You will learn how to upload files, connect to the cluster via SSH, launch jobs in Kubernetes, monitor execution, retrieve outputs, and clean up once finished.

For this tutorial, we will use the following example configuration:

- **username**: `emma`
- **NFS server IP**: `192.168.0.24`
- **port**: `3241`
- **NFS path**: `/nfs/bcb/emma`

---

## Overview of the Steps

1. Upload your R script, data, and Kubernetes manifest yaml file.
2. SSH into the gateway pod to access the cluster.
3. Set up your workspace and launch the Kubernetes job.
4. Monitor the job status and view logs.
5. Check for output files and retrieve them if needed.
6. Delete the job when done.

---

## Step 1: Uploading your Files to the Kubernetes Cluster

Before running the job, upload the necessary files, including your R script, data files, and job manifest, to the Kubernetes cluster using SFTP. The example files for this tutorial are already available in your bcb folder, and you can use them as a starting point to create your own.

You will find detailed upload instructions in our [official documentation](https://gitlab.com/fortunalab/kubernetes/-/blob/master/docs/sftp/readme.md), which includes various methods based on your operating system:

- **Nautilus** (Ubuntu/Debian)
- **Command Line** (Linux/Unix)
- **FileZilla** (Linux, Mac OS, Windows)
- **PowerShell** (Windows 10/11)

### Choosing the Right Method

Select the method best suited to your OS and follow the instructions. When uploading, place **all files in the `/home/emma/bcb` folder** on the server. This folder is persistent, meaning your files will not be lost even if the Kubernetes job completes or fails.

---

## Step 2: Connecting to the Kubernetes Gateway Pod

Now, you need to connect to the gateway pod (node with kubectl access) to launch and manage your jobs.

### SSH Access to the Gateway

Use the following command to connect via SSH. Replace `<port>`, `<your-username>` and `<gateway-ip>` with the values provided to you:
```bash
ssh -p <port> <your-username>@<gateway-ip>
```

For this tutorial, using the example values, the command would be:

```
ssh -p 3241 emma@192.168.0.24
```

Once connected, **go to the folder** where you uploaded your files:
```bash
cd /home/emma/bcb
```

This folder (`/home/emma/bcb`) will store input data and output results that you can later retrieve.

---

## Step 3: Understanding the Kubernetes Job Manifest File

The `example-job.yaml` file defines the job that will run your R script in multiple replicas.

- **Kubernetes Jobs**: A Kubernetes job creates one or more pods and ensures that a specified number of them successfully complete. For more information, check the [Kubernetes jobs documentation](https://kubernetes.io/docs/concepts/workloads/controllers/job/).

Here’s a breakdown of the main components of `example-job.yaml`:

### Key Sections of the Job Manifest

#### Job and Namespace
The job is defined with a `name` and `namespace` to organize resources.
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: emma-job
  namespace: emma-namespace  # please, replace 'emma' with your assigned namespace if different
```

#### Container Image
The container image in the manifest (`ebdbcb/r-clue`) includes the R environment required for your script. You can customize this image by building and uploading your own to Docker Hub. For more on Docker Hub, check [Docker Hub Documentation](https://docs.docker.com/docker-hub/).
```yaml
containers:
  - name: r
    image: ebdbcb/r-clue
```

#### Resource Quotas

Kubernetes restricts resources per namespace, so **make sure the resource limits do not exceed your assigned quota** to avoid job failures.

```yaml
resources:
  requests:
    cpu: "1"
    memory: "100Gi"
  limits:
    cpu: "2"
    memory: "110Gi"
```

Specify both requested and **maximum limits for CPU and memory** based on the available quota. Keep in mind that the requested resources and their limits specified in the spec: section of the container **apply to each replica of the job**. Therefore, you should adjust these parameters to optimize the number of replicas that can be run in parallel (a new replica will not be launched until the resources being used by another replica have been released).

#### Parallelism and Completions
This job runs with two active replicas in parallel (`parallelism: 2`) until a total of four replicas complete (`completions: 4`), enabling parallel processing.
```yaml
parallelism: 2
completions: 4
```

#### Command: Running the R Script

The `command` section copies your files to each pod's local folder, executes the R script, and saves outputs back to the `/bcb` folder for persistence.

```bash
command:
- /bin/bash
- -c
- |
  # ensure the scripts folder exists:
  mkdir -p /scripts

  # copy files from /bcb to pod's /scripts folder:
  cp -R /bcb/* /scripts/

  # extract pod index to identify pod for output file:
  pod_id=$(echo $MY_POD_NAME | sed 's/.*-\([0-9]\+\)-.*/\1/')

  # run the R script with the pod ID as an argument:
  R -e "args <- commandArgs(trailingOnly = TRUE); source('/scripts/script.R')" --args "$pod_id"
  
  # copy outputs from pod's /scripts folder back to /bcb:
  cp /scripts/output*.txt /bcb/
```

Here is the explanation of the code lines in the `command`: field:

1. Ensure the **scripts folder** exists:

```bash
mkdir -p /scripts
```

This command creates a directory named `/scripts` within the pod. The `-p` option ensures that no error is thrown if the directory already exists, allowing the script to run smoothly without interruption.

2. **Copy files from /bcb** to pod's `/scripts` folder:

```bash
cp -R /bcb/* /scripts/
```

This command copies all files and subfolders from the /bcb folder (where input files are stored) to the newly created `/scripts` folder in the pod. The -R flag allows for recursive copying, ensuring that all contents are included.

3. Extract **pod index** to identify pod for output file:

```bash
pod_id=$(echo $MY_POD_NAME | sed 's/.*-\([0-9]\+\)-.*/\1/')
```

This line uses the echo command to retrieve the name of the pod from the environment variable `$MY_POD_NAME` and processes it with `sed`. The sed command extracts the pod index number from the pod name, which is used later to create a unique output file name. This helps identify which specific instance of the script generated the output.

4. **Run the R script** with the pod ID as an argument:

```bash
R -e "args <- commandArgs(trailingOnly = TRUE); source('/scripts/script.R')" --args "$pod_id"
```

This command executes the R script located in the `/scripts` folder. The `-e` flag allows for R expressions to be executed from the command line. The script receives the `pod_id` as an argument, enabling it to customize its output based on which pod instance is running.

5. **Copy outputs to /bcb folder** from pod's `/scripts`:

```bash
cp /scripts/output*.txt /bcb/
```

This command copies any output files generated by the R script from the pod's `/scripts` directory back to the `/bcb` folder. The `output*.txt` pattern ensures that all text files matching that format are included, allowing the results to persist in the original storage location.

**Tips:** The lines of code in the command: field could alternatively be placed in a bash file called `run.sh`, which can be executed from the `command:` field, allowing you to avoid editing this section for future jobs.

Example of `run.sh`:

```bash
#!/bin/bash

# create scripts folder if it doesn't exist:
mkdir -p /scripts

# copy files from bcb folder to local folder:
cp -R /bcb/* /scripts/

# read pod index to point at the right input:
pod_id=$(echo $MY_POD_NAME | sed 's/.*-\([0-9]\+\)-.*/\1/')

# run scripts:
R -e "args <- commandArgs(trailingOnly = TRUE); source('/scripts/script.R')" --args "$pod_id"

# copy results from local folder to bcb folder:
cp /scripts/output*.txt /bcb/

```

Updated `command:` field in the YAML File:

```yaml
command:
- /bin/bash
- -c
- "/scripts/run.sh"
```

In this setup, the `run.sh` script contains all the necessary commands, allowing you to maintain and update the job logic without modifying the `yaml` file directly.


#### Understanding the code of script.R

This section aims to demystify the code in `script.R` and guide users on how to adapt it for their own needs. By understanding each part, users can confidently modify the script and integrate their own logic!

Let us break down what this R script does step by step. It is designed to be run in a Kubernetes environment, where each replica of the job can perform its tasks independently while keeping track of its own identity.

**Note:** This tutorial does not aim to teach the user how to program in R; instead, it serves as a guide for adapting the code to execute multiple replicas using Kubernetes jobs.

```r
# get script arguments:
args <- commandArgs(trailingOnly = TRUE)

# check if pod_id is provided:
if (length(args) == 0) {
  stop("No pod_id argument provided.")
}

# assign pod_id from arguments:
pod_id <- args[1]

# create output file name with pod_id:
output_file <- paste0("/scripts/output-", pod_id, ".txt")

# write something to the file:
write(paste("Writting something from pod ID:", pod_id), file = output_file)

# your code here: place your custom R code below this line:
# example:
# result <- someFunction()
# write(result, file = output_file)

# print success message
cat("pod_id written to file:", output_file, "\n")
```

1. Getting Script **Arguments**

```r
args <- commandArgs(trailingOnly = TRUE)
```

This line retrieves any arguments passed to the script when it is executed. The `commandArgs()` function allows the script to access parameters that are specified at the command line, making it flexible for different runs.

2. Checking for **pod_id**

```r
if (length(args) == 0) {
  stop("No pod_id argument provided.")
}
```

Here, the script checks if an argument has been provided. If no arguments are found, it stops execution and gives an error message. This is important because the script relies on having a `pod_id` to identify which replica is currently running.

Based on the value of pod_id, the user can modify the script to either read parameter values from a file for the current replica's calculations or dynamically generate these values based on that index.


```r
pod_id <- args[1]
```

The script assigns the first argument (which should be the pod_id) to a variable named pod_id. This pod_id is used later in the script to create unique output files for each pod.

3. Creating the **Output File Name**

```r
output_file <- paste0("/scripts/output-", pod_id, ".txt")
```

This line generates a filename that incorporates the pod_id, resulting in a unique output file for each pod. For example, if the pod_id is 0, the file will be named `/scripts/output-0.txt`.

4. **Writing** to the Output File

```r
write(paste("Writting something from Pod ID:", pod_id), file = output_file)
```

In this step, the script writes a message to the output file that includes the pod_id. This allows users to know which pod generated which output, helping with tracking and debugging.

5. **User Code** Placeholder

```r
# your code here: place your custom R code below this line
# example:
# result <- someFunction()
# write(result, file = output_file)
```

This comment serves as a reminder for users to insert their custom R code here. It encourages them to modify the script to perform their specific tasks, with an example showing how results can be written to the same output file.

6. Printing a **Success Message**

```r
cat("pod_id written to file:", output_file, "\n")
```

Finally, the script prints a success message to the console, confirming that the pod_id has been successfully recorded in the output file. This feedback is helpful for users to verify that their script ran as intended.

**Tips:** The user is responsible for ensuring that their code saves the results stored in variables in a suitable format, allowing them to be easily retrieved and utilized in another job or script that requires these results as input data. This practice promotes seamless integration and efficient data handling across multiple processes.

#### Backoff Limit

This setting controls how many times Kubernetes retries the job if it fails, in this case, up to four retries `(backoffLimit: 4)`.

For example, if one of the pods (a replica of the script) fails three times, Kubernetes will attempt to restart that pod for a total of four attempts. If the pod fails again on the fourth attempt, Kubernetes will stop trying to restart it, and the job will be marked as failed.

---

## Step 4: Launching the Job

Once the files are uploaded and manifest understood, run the job with the following command:

```bash
kubectl apply -f example-job.yaml
```

This command tells Kubernetes to create the job specified in `example-job.yaml`, which will launch multiple replicas of your R script.

For more information about the kubectl tool, visit the [official Kubernetes documentation](https://kubernetes.io/docs/reference/kubectl/).

---

## Step 5: Monitoring Job Status and Logs

To verify that your job is running, check the status of the pods:

```bash
kubectl get pods -n emma-namespace
```

Example output:

```bash
NAME                 READY   STATUS    RESTARTS   AGE
clue-model-0         1/1     Running   0          2m
clue-model-1         1/1     Running   0          2m
clue-model-2         1/1     Pending   0          2m
clue-model-3         0/1     Pending   0          2m
```

In this example:

- **Pod `clue-model-0`** and **`clue-model-1`** are successfully running.

- **Pod `clue-model-2`** and **`clue-model-3`** are still pending, which could mean they are waiting for resources or for other pods to complete.

### Viewing Logs

To monitor the output of each pod, use the `kubectl logs` command 

```bash
kubectl logs <pod_name> -n emma-namespace
```

substituting `<pod-name>` with one of the pod names displayed in the previous command: output:

```bash
kubectl logs clue-model-0 -n emma-namespace
```

Example output:

```bash
R version 4.4.1 (2024-06-14) -- "Race for Your Life"
Copyright (C) 2024 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> args <- commandArgs(trailingOnly = TRUE); source('/scripts/script.R')
pod_id written to file: /scripts/output-0.txt 
> 
> 
```

**Tip**: This step can help troubleshoot any issues by reviewing log output directly. You would need to correct any possible errors in your R code, which may require checking the logs as explained above to identify the issues. If the source of the problem is beyond your understanding, you should contact the administrators for assistance. You can use the following command to view any errors that occurred at the Kubernetes level for that specific pod or job:

```bash
kubectl describe pod <pod-name> -n emma-namespace
```

---

## Step 6: Verifying Output Files

When the job finishes, you can confirm the generated output files in the folder `/home/emma/bcb`. Use:

```bash
ls -lt /home/emma/bcb
```

This command lists files in order of modification time, with the latest at the top. You should see output files named like output-0.txt, output-1.txt, output-2.txt, and output-3.txt, corresponding to each replica of the job.

Alternatively, you can use SFTP to retrieve output files, following the same steps as in Step 1.

---

## Step 7: Cleaning Up Resources

Once you've reviewed the outputs, delete the job to free up resources:
```bash
kubectl delete job clue-model -n emma-namespace
```

This removes the job from emma-namespace, but any files in the folder `/home/emma/bcb` will remain intact.

---

## Conclusion

With these steps, you can effectively run multiple replicas of your R script on Kubernetes, monitor execution, and retrieve persistent outputs. This setup enables reproducible, scalable R processing workflows in our Kubernetes environment. 

Enjoy experimenting with Kubernetes jobs, and do not hesitate to adjust the `yaml` settings for different processing needs!
