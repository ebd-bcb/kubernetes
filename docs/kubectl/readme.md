# Operating the BCB lab Cluster Using kubectl
> provided by: [Raúl Ortega](https://gitlab.com/raul.ortega)

## Objective

This tutorial will guide you through setting up **kubectl** on a GNU/Linux or Unix system (Ubuntu 22.04 as an example) and using it to interact with the **BCB lab Kubernetes cluster**. You will learn how to install kubectl, configure it using the provided config file, and execute common commands to manage your resources. For instructions on installing and using kubectl on other operating systems, please visit the official Kubernetes documentation.

## Requirements

- **kubectl** installed on your machine. Visit the [official installation guide](https://kubernetes.io/docs/tasks/tools/install-kubectl/) to install it.
- Access to the **BCB lab cluster** via the correct IP and port (in this example, we’ll use **192.168.0.24** as the host and **16443** as the API port).
- The **Kubeconfig file** provided by the BCB lab or downloaded from the SFTP server.

## Step 1: Installing kubectl on GNU/Linux

### Method 1: Generic Installation Using curl

1. **Download the latest release of kubectl (for x86-64 architecture):**
   ```bash
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
   ```

   *To download for ARM64 architecture, replace the architecture part in the URL:*
   ```bash
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/arm64/kubectl"
   ```

2. **Optional: Validate the binary by downloading the checksum file and verifying it:**
   ```bash
   curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
   echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
   ```

3. **Install kubectl:**
   ```bash
   sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   ```

   *If you don’t have root access, install to a local directory:*
   ```bash
   chmod +x kubectl
   mkdir -p ~/.local/bin
   mv ./kubectl ~/.local/bin/kubectl
   ```

4. **Test your installation:**
   ```bash
   kubectl version --client
   ```

### Method 2: Installation for Debian-based Distributions

1. **Update the apt package index and install dependencies:**
   ```bash
   sudo apt-get update
   sudo apt-get install -y apt-transport-https ca-certificates curl gnupg
   ```

2. **Download the Kubernetes signing key and add it to your keyring:**
   ```bash
   sudo mkdir -p /etc/apt/keyrings
   curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.31/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
   sudo chmod 644 /etc/apt/keyrings/kubernetes-apt-keyring.gpg
   ```

3. **Add the Kubernetes apt repository:**
   ```bash
   echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.31/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
   sudo chmod 644 /etc/apt/sources.list.d/kubernetes.list
   ```

4. **Update the apt package index and install kubectl:**
   ```bash
   sudo apt-get update
   sudo apt-get install -y kubectl
   ```

## Step 2: Configuring kubectl with the BCB lab Cluster

### Modifying the Configuration File

**Note:** When you receive the configuration file from the BCB lab, it won't need to be updated with the correct **IP address** and **port** for the cluster’s API server.

However, if you didn't receive the configuration file from the BCB lab, you can download it from the SFTP server:

1. **Access your SFTP server** as explained [here](https://gitlab.com/ebd-bcb/kubernetes/-/blob/master/docs/sftp/readme.md).
2. **Show hidden files** on your system to reveal the `.kube` folder, which contains the config file.

   ![Show hidden files](kubectl_show_hidden_files.png)

3. **Navigate to the .kube folder** and **download the config** file.

4. **Locate** the section in the config file that contains the cluster’s address. It will look something like this:

    ```yaml
    clusters:
    - cluster:
        server: https://<old-ip>:<old-port>
    ```

5. **Change the IP and port** to match the following values:

    ```yaml
    clusters:
    - cluster:
        server: https://192.168.0.24:16443
    ```

6. **Save the file** in the `/home/emma/.kube/` folder. Kubectl will now automatically detect it.


5. **Verify kubectl installation:**
   ```bash
   kubectl version --client
   ```

### Verifying kubectl Configuration

After installation, make sure kubectl is properly configured by checking your cluster state:

```bash
kubectl cluster-info
```

For more detailed installation instructions, please visit the [kubectl installation page](https://kubernetes.io/docs/tasks/tools/install-kubectl/).


## Step 3: Basic kubectl Commands

Now that kubectl is configured, you can use the following commands to interact with your namespace (in this case, `emma-namespace`).

### 1. **Check Your Resource Quota**

To view the resource quota assigned to your namespace:

```bash
kubectl get resourcequota -n emma-namespace
```

Example output:

```bash
NAME             CREATED AT
emma-quota       2023-10-11T10:00:00Z
```

### 2. **View Pods in Your Namespace**

To list all running pods in your namespace:

```bash
kubectl get pods -n emma-namespace
```

Example output:

```bash
NAME                         READY   STATUS    RESTARTS   AGE
emma-gateway-bcb-79899445ff-n8mn2    1/1     Running   0          3d
emma-rstudio                         1/1     Running   0          3d
```

### 3. **Check Services and Their Ports**

To get a list of services and their associated ports and IP addresses:

```bash
kubectl get services -n emma-namespace
```

Example output:

```bash
NAME          TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
emma-service  ClusterIP   10.96.0.1     <none>        80/TCP    3d
```

### 4. **View Jobs**

To list all batch jobs running in your namespace:

```bash
kubectl get jobs -n emma-namespace
```

Example output:

```bash
NAME                    COMPLETIONS   DURATION   AGE
emma-job-123           1/1           30s        1h
```

## Step 4: Additional Useful Commands

In addition to basic commands, there are a few more advanced operations you can perform using kubectl.

### 1. **Access a Pod’s Terminal**

You can open a shell session inside a running pod with the following command:

```bash
kubectl exec -it <pod-name> -n emma-namespace -- /bin/bash
```

Example:

```bash
kubectl exec -it emma-gateway-bcb-79899445ff-n8mn2 -n emma-namespace -- /bin/bash
```

### 2. **View Logs of a Pod**

To see the logs of a specific pod, use:

```bash
kubectl logs <pod-name> -n emma-namespace
```

Example:

```bash
kubectl logs emma-gateway-bcb-79899445ff-n8mn2 -n emma-namespace
```

### 3. **Copy Files from Local Machine to a Pod**

To copy files from your local machine to a pod:

```bash
kubectl cp /path/to/local/file <pod-name>:/path/in/pod -n emma-namespace
```

Example:

```bash
kubectl cp /home/emma/data.txt emma-gateway-bcb-79899445ff-n8mn2:/app/data.txt -n emma-namespace
```

### 4. **Check Cluster Nodes**

Although your access is limited to your namespace, you can still view information about the nodes in the cluster:

```bash
kubectl get nodes
```

Example output:

```bash
NAME           STATUS   ROLES           AGE     VERSION
node-1         Ready    control-plane   10d     v1.22.0
```

## Step 5: Other Permissions and Actions

Based on your role and permissions, you have access to a broader set of resources. Here’s a summary of the resources you can interact with:

- **Pods**, **events**, **persistent volumes**, **services**, and **secrets**.
- **Exec into pods**, view **logs**, and interact with **configmaps** and **endpoints**.
- Access to **jobs** and **resource quotas** in your namespace.
- Permissions for **storage-related resources** such as **storage classes** and **volume attachments**.

You can explore the full list of kubectl commands in the [official documentation](https://kubernetes.io/docs/reference/kubectl/overview/).

## Conclusion

This tutorial provided an overview of how to install and configure kubectl for accessing the BCB lab Kubernetes cluster. You learned basic kubectl commands to manage your namespace, including checking resource quotas, viewing pods and jobs, and working with services.

For more advanced usage and additional commands, please refer to the [official Kubernetes documentation](https://kubernetes.io/docs/home/).

